<?php
use Migrations\AbstractMigration;

class AddSpriteToPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pokes');
        $table->addColumn('sprite', 'string', [
            'default' => 'https://generation-nintendo.com/images/encyclopedie/2-1417030962.png',
            'limit' => 200,
            'null' => false,
        ]);
        $table->update();
    }
}
