<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();

            #region Recup tableau des  pokes favoris des dresseurs
            $dresseur1 = $this->Fights->FirstDresseurs->get($formData['first_dresseur_id'], [
                'contain' => ['DresseurPokes.Pokes' => function ($query) {
                    return $query->where(['is_fav' => true]);
                }]
            ]);

            $dresseur2 = $this->Fights->SecondDresseurs->get($formData['second_dresseur_id'], [
                'contain' => ['DresseurPokes.Pokes' => function ($query) {
                    return $query->where(['is_fav' => true]);
                }]
            ]);
            #endregion

            if ($formData['first_dresseur_id'] == $formData['second_dresseur_id'] or empty($dresseur1->dresseur_pokes[0]) or empty($dresseur2->dresseur_pokes[0])) {
                $this->Flash->error(__("The fight could not be saved, same player selected or one of those dresseurs doesn't have a favorit pokemon. Please, try again."));
            } else {
                $formData['winner_dresseur_id']  = $this->_fight($formData);
                $fight = $this->Fights->patchEntity($fight, $formData);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(__('The fight has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The fight could not be saved. Please, try again.'));
            }
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }

    protected function _tour($dresseur1, $dresseur2)
    {
        Log::write('info', "=============");
        Log::write('info', "Début du Tour");
        // Atk dresseur2 -> dresseur1
        // Test touché
        if (rand(0, 100) >= 5) {
            Log::write('info', "Pokémon1 touché");
            // Test défense pokémon attaqué
            if (rand(0, 100 >= 90)) {
                Log::write('info', "Pokémon1 se défend");
                $dresseur1->dresseur_pokes[0]->poke->Hp -= ($dresseur2->dresseur_pokes[0]->poke->Atk) / (($dresseur1->dresseur_pokes[0]->poke->Dfns) * 0.5);
            } else {
                $dresseur1->dresseur_pokes[0]->poke->Hp -= $dresseur2->dresseur_pokes[0]->poke->Atk;
            }
        } else {
            Log::write('info', "Pokémon1 ESQUIVE !!");
        }

        // Atk dresseur1 -> dresseur2
        // Test touché
        if (rand(0, 100) >= 5) {
            Log::write('info', "Pokémon2 touché");
            // Test défense pokémon attaqué
            if (rand(0, 100 >= 90)) {
                Log::write('info', "Pokémon2 se défend");
                $dresseur2->dresseur_pokes[0]->poke->Hp -= ($dresseur1->dresseur_pokes[0]->poke->Atk) / (($dresseur2->dresseur_pokes[0]->poke->Dfns) * 0.5);
            } else {
                $dresseur2->dresseur_pokes[0]->poke->Hp -= $dresseur1->dresseur_pokes[0]->poke->Atk;
            }
        } else {
            Log::write('info', "Pokémon2 ESQUIVE !!");
        }

        Log::write('info', $dresseur1->dresseur_pokes[0]->poke->Hp);
        Log::write('info', $dresseur2->dresseur_pokes[0]->poke->Hp);

        Log::write('info', "Fin du Tour");
        if ($dresseur1->dresseur_pokes[0]->poke->Hp > 0 and $dresseur2->dresseur_pokes[0]->poke->Hp > 0)
            return $this->_tour($dresseur1, $dresseur2);

        if ($dresseur1->dresseur_pokes[0]->poke->Hp <= 0 and $dresseur2->dresseur_pokes[0]->poke->Hp <= 0)
            if (rand(0, 100) <= 49)
                return true;
            else
                return false;

        if ($dresseur1->dresseur_pokes[0]->poke->Hp <= 0)
            return false;
        else
            return true;
    }

    protected function _fight($formData)
    {
        $dresseur1 = $this->Fights->FirstDresseurs->get($formData['first_dresseur_id'], [
            'contain' => ['DresseurPokes.Pokes' => function ($query) {
                return $query->where(['is_fav' => true]);
            }]
        ]);

        $dresseur2 = $this->Fights->SecondDresseurs->get($formData['second_dresseur_id'], [
            'contain' => ['DresseurPokes.Pokes' => function ($query) {
                return $query->where(['is_fav' => true]);
            }]
        ]);

        $res_fight = $this->_tour($dresseur1, $dresseur2);

        if ($res_fight) {
            $this->Flash->success(__("Le dresseur " . $dresseur1->last_name . " à gagné"));
            return $formData['first_dresseur_id'];
        } else {
            $this->Flash->success(__("Le dresseur " . $dresseur2->last_name . " à gagné"));
            return $formData['second_dresseur_id'];
        }
    }


    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
