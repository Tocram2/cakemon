<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DresseurPokes Controller
 *
 * @property \App\Model\Table\DresseurPokesTable $DresseurPokes
 *
 * @method \App\Model\Entity\DresseurPoke[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseurPokesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dresseurs', 'Pokes']
        ];
        $dresseurPokes = $this->paginate($this->DresseurPokes);

        $this->set(compact('dresseurPokes'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur Poke id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseurPoke = $this->DresseurPokes->get($id, [
            'contain' => ['Dresseurs', 'Pokes', 'is_fav']
        ]);

        $this->set('dresseurPoke', $dresseurPoke);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseurPoke = $this->DresseurPokes->newEntity();
        if ($this->request->is('post')) {
            $dresseurPoke = $this->DresseurPokes->patchEntity($dresseurPoke, $this->request->getData());
            if ($this->DresseurPokes->save($dresseurPoke)) {
                $this->Flash->success(__('The dresseur poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur poke could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokes->Dresseurs->find('list', ['limit' => 200]);
        $pokes = $this->DresseurPokes->Pokes->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPoke', 'dresseurs', 'pokes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur Poke id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseurPoke = $this->DresseurPokes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseurPoke = $this->DresseurPokes->patchEntity($dresseurPoke, $this->request->getData());
            if ($this->DresseurPokes->save($dresseurPoke)) {
                $this->Flash->success(__('The dresseur poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur poke could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokes->Dresseurs->find('list', ['limit' => 200]);
        $pokes = $this->DresseurPokes->Pokes->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPoke', 'dresseurs', 'pokes'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur Poke id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseurPoke = $this->DresseurPokes->get($id);
        if ($this->DresseurPokes->delete($dresseurPoke)) {
            $this->Flash->success(__('The dresseur poke has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseur poke could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
