<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Http\Client;

/**
 * Import shell command.
 */
class ImportShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main($id_poke)
    {
        $url = "https://pokeapi.co/api/v2/pokemon/";
        $connection = ConnectionManager::get('default');
        $http = new Client();
        $id = $http->get($url);
        $nbPoke=649;
        if(empty($id->getJson()))
        {
            $this->quiet("Échec importation depuis l'API");
            return false;
        }
        else if ($id_poke <= $nbPoke && $id_poke > 0)
        {
                $json = $http->get($url.strval($id_poke));
                $poke = $json->getJson();
                $poketab = TableRegistry::getTableLocator()->get('Pokes');
                $enregistrement = $poketab->newEntity();
                $enregistrement->name = $poke["name"];
                $enregistrement->pokedex_number = $poke["id"];
                $enregistrement->Hp = $poke['stats']["5"]["base_stat"];
                $enregistrement->Atk =$poke['stats']["4"]["base_stat"];
                $enregistrement->Dfns =$poke['stats']["3"]["base_stat"];
                $enregistrement->Spd =$poke['stats']["2"]["base_stat"];
                $enregistrement->sprite = $poke['sprites']['front_default'];
                
                $this->quiet("Pokémon inséré : ".$id_poke." " . $enregistrement);
                if ($poketab->save($enregistrement)) {
                    // L'entity $enregistrement contient maintenant l'id
                    $id = $enregistrement->id;
                    return true;
                }
        }
        else
        {
            $this->quiet("Numéro de Pokémon invalide");
            return false;
        }
    }
}
