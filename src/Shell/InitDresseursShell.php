<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

/**
 * InitDresseurs shell command.
 */
class InitDresseursShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $FirstName = array('Black', 'Brown', 'White');
        $Name = array('Text', 'Cookie', 'Chocolate');
        for($i=0;$i<3;$i++){
            $insertion = TableRegistry::getTableLocator()->get('dresseurs');
            $enregistrement = $insertion->newEntity();
            $enregistrement->first_name = $FirstName[$i];
            $enregistrement->last_name = $Name[$i];
            $insertion->save($enregistrement);
        }
    }
}
