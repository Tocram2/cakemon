<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;

/**
 * ImportLotsaPokes shell command.
 */
class ImportLotsaPokesShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $url = "https://pokeapi.co/api/v2/pokemon/";
        $connection = ConnectionManager::get('default');
        $http = new Client();
        $id = $http->get($url);
        $nbPoke=649;
        if(empty($id->getJson()))
        {
            $this->quiet("Échec importation depuis l'API");
            return;
        }
        else
        {
            for($i=1; $i<=6; $i++)
            {
                $rand = rand(1, $nbPoke);
                $json = $http->get($url.strval($rand));
                $poke = $json->getJson();
                $poketab = TableRegistry::getTableLocator()->get('Pokes');
                $enregistrement = $poketab->newEntity();
                $enregistrement->name = $poke["name"];
                $enregistrement->pokedex_number = $poke["id"];
                $enregistrement->health = $poke['stats']["5"]["base_stat"];
                $enregistrement->attack =$poke['stats']["4"]["base_stat"];
                $enregistrement->defense =$poke['stats']["3"]["base_stat"];

                $this->quiet("Pokemon inserted : ".$rand." " . $enregistrement);
                if ($poketab->save($enregistrement)) {
                    // L'entity $enregistrement contient maintenant l'id
                    $id = $enregistrement->id;
                }
            }
        }
    }

}
