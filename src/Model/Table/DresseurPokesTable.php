<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DresseurPokes Model
 *
 * @property \App\Model\Table\DresseursTable&\Cake\ORM\Association\BelongsTo $Dresseurs
 * @property \App\Model\Table\PokesTable&\Cake\ORM\Association\BelongsTo $Pokes
 *
 * @method \App\Model\Entity\DresseurPoke get($primaryKey, $options = [])
 * @method \App\Model\Entity\DresseurPoke newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DresseurPoke[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPoke|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseurPoke saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseurPoke patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPoke[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPoke findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DresseurPokesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dresseur_pokes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Dresseurs', [
            'foreignKey' => 'dresseur_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Pokes', [
            'foreignKey' => 'poke_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_fav')
            ->notEmptyString('is_fav');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dresseur_id'], 'Dresseurs'));
        $rules->add($rules->existsIn(['poke_id'], 'Pokes'));

        return $rules;
    }
}
