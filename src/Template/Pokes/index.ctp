<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?></li>
    </ul>
</nav>

<!-- Nouvel affichage -->
<div class="pokes index large-4 medium-5 small-6 grid content">
    <h3><?= __('Pokes') ?></h3>
    <div class='container'>
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <?php foreach ($pokes as $poke) : ?>
                        <li class="col-md-3">
                            <div class='front'>
                                <h4><?= h($poke->name) ?></h4>
                                <img src='<?= h($poke->sprite) ?>'>
                                <div id="infos-poke">
                                    <p>ID: <?= $this->Number->format($poke->id) ?></p>
                                    <p>Num. Pokédex: <?= $this->Number->format($poke->pokedex_number) ?></p>
                                    <p>PV: <?= $this->Number->format($poke->Hp) ?></p>
                                    <p>Attaque: <?= $this->Number->format($poke->Atk) ?></p>
                                    <p>Défense: <?= $this->Number->format($poke->Dfns) ?></p>
                                    <p>Vitesse: <?= $this->Number->format($poke->Spd) ?></p>
                                </div>

                                <div id="actions-poke">
                                    <?= $this->Html->link(__('Voir'), ['action' => 'view', $poke->id]) ?>
                                    <a> · </a>
                                    <?= $this->Html->link(__('Éditer'), ['action' => 'edit', $poke->id]) ?>
                                    <a> · </a>
                                    <?= $this->Form->postLink(__('Suppr.'), ['action' => 'delete', $poke->id], ['confirm' => __('Voulez-vous vraiment supprimer {0} ?', $poke->name)]) ?>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Ancienne méthode d'affichage
<div class="pokes index large-9 medium-8 columns content">
    <h3><?= __('Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pokedex_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Hp') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Atk') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Dfns') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Spd') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pokes as $poke) : ?>
            <tr>
                <td><?= $this->Number->format($poke->id) ?></td>
                <td><?= h($poke->name) ?></td>
                <td><?= $this->Number->format($poke->pokedex_number) ?></td>
                <td><?= h($poke->created) ?></td>
                <td><?= h($poke->modified) ?></td>
                <td><?= $this->Number->format($poke->Hp) ?></td>
                <td><?= $this->Number->format($poke->Atk) ?></td>
                <td><?= $this->Number->format($poke->Dfns) ?></td>
                <td><?= $this->Number->format($poke->Spd) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
-->