<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPoke[]|\Cake\Collection\CollectionInterface $dresseurPokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurPokes index large-9 medium-8 columns content">
    <h3><?= __('Dresseur Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('poke_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_fav') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurPokes as $dresseurPoke): ?>
            <tr>
                <td><?= $this->Number->format($dresseurPoke->id) ?></td>
                <td><?= $dresseurPoke->has('dresseur') ? $this->Html->link($dresseurPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPoke->dresseur->id]) : '' ?></td>
                <td><?= $dresseurPoke->has('poke') ? $this->Html->link($dresseurPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPoke->poke->id]) : '' ?></td>
                <td><?= h($dresseurPoke->created) ?></td>
                <td><?= h($dresseurPoke->modified) ?></td>
                <td><?= h($dresseurPoke->is_fav) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPoke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPoke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPoke->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
