<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur'), ['action' => 'edit', $dresseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokes'), ['controller' => 'DresseurPokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Poke'), ['controller' => 'DresseurPokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->first_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($dresseur->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($dresseur->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseur->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseur Pokes') ?></h4>
        <?php if (!empty($dresseur->dresseur_pokes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dresseur->dresseur_pokes as $dresseurPokes): ?>
            <tr>
                <td><?= h($dresseurPokes->id) ?></td>
                <td><?= h($dresseurPokes->dresseur_id) ?></td>
                <td><?= h($dresseurPokes->poke_id) ?></td>
                <td><?= h($dresseurPokes->created) ?></td>
                <td><?= h($dresseurPokes->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokes', 'action' => 'view', $dresseurPokes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokes', 'action' => 'edit', $dresseurPokes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokes', 'action' => 'delete', $dresseurPokes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
