<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Fights'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List First Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New First Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fights form large-9 medium-8 columns content">
    <?= $this->Form->create($fight) ?>
    <fieldset>
        <legend><?= __('Add Fight') ?></legend>
        <?php
            echo $this->Form->control('first_dresseur_id', ['options' => $firstDresseurs]);
            echo $this->Form->control('second_dresseur_id', ['options' => $secondDresseurs]);
            //echo $this->Form->control('winner_dresseur_id', ['options' => $winnerDresseurs]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
