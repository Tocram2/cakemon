<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseursTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseursTable Test Case
 */
class DresseursTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseursTable
     */
    public $Dresseurs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Dresseurs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Dresseurs') ? [] : ['className' => DresseursTable::class];
        $this->Dresseurs = TableRegistry::getTableLocator()->get('Dresseurs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Dresseurs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
